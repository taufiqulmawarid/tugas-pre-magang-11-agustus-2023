import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_api_pre_magang/blocs/todos_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a blue toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: BlocProvider(
        create: (context) => TodosBloc(),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // final todosBloc = BlocProvider.of<TodosBloc>(context);
    final todosBloc = BlocProvider.of<TodosBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tugas 11 Agustus'),
      ),
      body: BlocBuilder<TodosBloc, TodosState>(
        builder: (context, state) {
          return state.maybeMap(
            loading: (_) => const Center(child: CircularProgressIndicator(),),
            loaded: (todos) {
              return ListView.builder(
                itemCount: todos.todos.length,
                itemBuilder: (context, index) {
                  final todo = todos.todos[index];
                  return ListTile(
                    title: Text(todo.title ?? ''),
                    subtitle: Text(todo.completed ?? ''),
                  );
                },
              );
            },
            error: (errorMessage) {
              return Center(child: Text(errorMessage.message),);
            },
            orElse: () => Container(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          todosBloc.add(const TodosEvent.fetchTodos());
        },
        child: const Icon(Icons.refresh),
      ),
    );
  }
}