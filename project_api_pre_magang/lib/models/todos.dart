import 'package:freezed_annotation/freezed_annotation.dart';

part 'todos.freezed.dart';
part 'todos.g.dart';

@Freezed()
class Todos with _$Todos {
  const factory Todos({
    required int? userId,
    required int? id,
    required String? title,
    required String? completed
  }) = _Todos;

  factory Todos.fromJson(Map<String, dynamic> json) => _$TodosFromJson(json);
}