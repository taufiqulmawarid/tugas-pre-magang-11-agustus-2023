import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:dio/dio.dart';
import 'package:project_api_pre_magang/models/todos.dart';

part 'todos_event.dart';
part 'todos_state.dart';
part 'todos_bloc.freezed.dart';

class TodosBloc extends Bloc<TodosEvent, TodosState> {
  final Dio _dio = Dio();
  TodosBloc() : super(const TodosState.initial()) {
    on<TodosEvent>((event, emit) async {
      // TODO: implement event handler
      print("test");
    });
  }

  Stream<TodosState> mapEventToState(TodosEvent event) async* {
    yield* event.map(
      fetchTodos: (_) async* {
        print("fetching todos");
        yield const TodosState.loading();
        try {
          final response = await _dio.get('https://jsonplaceholder.typicode.com/todos');
          final List<Todos> todos = List<Todos>.from(response.data.map((x) => Todos.fromJson(x)));
          yield TodosState.loaded(todos);
        } catch (e) {
          yield const TodosState.error('Error happened');
        }
      }
    );
  }
}
